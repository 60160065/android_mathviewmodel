package buu.phatcharapol.calculator

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PlusViewModelFactory(private val lastCorrect: Int, private val lastIncorrect: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlusViewModel::class.java)) {
            return PlusViewModel(lastCorrect, lastIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
