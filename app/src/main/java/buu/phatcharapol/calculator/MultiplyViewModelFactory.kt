package buu.phatcharapol.calculator

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MultiplyViewModelFactory(private val lastCorrect: Int, private val lastIncorrect: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MultiplyViewModel::class.java)) {
            return MultiplyViewModel(lastCorrect, lastIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
