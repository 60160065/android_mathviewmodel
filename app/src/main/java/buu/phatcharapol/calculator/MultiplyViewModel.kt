package buu.phatcharapol.calculator

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class MultiplyViewModel(lastCorrect: Int, lastIncorrect: Int) : ViewModel() {

    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct
    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect
    private val _ansNumber = MutableLiveData<Int>()
    val ansNumber: LiveData<Int>
        get() = _ansNumber
    private val _ranNum1 = MutableLiveData<Int>()
    val ranNum1: LiveData<Int>
        get() = _ranNum1
    private val _ranNum2 = MutableLiveData<Int>()
    val ranNum2: LiveData<Int>
        get() = _ranNum2
    private val _randomChoice = MutableLiveData<Int>()
    val randomChoice: LiveData<Int>
        get() = _randomChoice
    private val _choice1 = MutableLiveData<Int>()
    val choice1: LiveData<Int>
        get() = _choice1
    private val _choice2 = MutableLiveData<Int>()
    val choice2: LiveData<Int>
        get() = _choice2
    private val _choice3 = MutableLiveData<Int>()
    val choice3: LiveData<Int>
        get() = _choice3



    init {
        _correct.value = lastCorrect
        _incorrect.value = lastIncorrect
        _ansNumber.value = 0
        _ranNum1.value = 0
        _ranNum2.value = 0
        _randomChoice.value = 0
        startGame()
        Log.i("MultiplyViewModel", "MultiplyViewModel created!")
    }
    override fun onCleared() {
        super.onCleared()
        Log.i("MultiplyViewModel", "MultiplyViewModel destroyed!")
    }

    fun startGame() {
        _ansNumber.value = randomNumber()
        _randomChoice.value = choice()
        randomButton()
    }

    private fun randomNumber(): Int {
        _ranNum1.value = (0 until 10).random()
        _ranNum2.value = (0 until 10).random()
        return _ranNum1.value!! * _ranNum2.value!!
    }

    fun scoreUp() {
        _correct.value = _correct.value?.plus(1)
    }
    fun scoreDown() {
        _incorrect.value = _incorrect.value?.plus(1)
    }

    private fun choice(): Int {
        return (1 until 4).random()
    }

    fun randomButton() {
        if (randomChoice.value == 1) {
            _choice1.value = _ansNumber.value!!
            _choice2.value = (_ansNumber.value!! + 1)
            _choice3.value = (_ansNumber.value!! + 2)
        } else if (randomChoice.value == 2) {
            _choice1.value = (_ansNumber.value!! - 1)
            _choice2.value = _ansNumber.value!!
            _choice3.value = (_ansNumber.value!! + 1)
        } else if (randomChoice.value == 3) {
            _choice1.value = (_ansNumber.value!! - 2)
            _choice2.value = (_ansNumber.value!! - 1)
            _choice3.value = _ansNumber.value!!
        }
    }
}